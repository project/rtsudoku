/**
 * @file
 * RTSudoku js file
 */

(function($) {
  Drupal.behaviors.rtSudoku = {
    attach : function(context, settings) {
      // Set up vars
      $counter = 0;
      $errors = 0;
      countdown = 0;
      cdstring = '';
      contin = true;
      duration = false;
      $keepgo = true;
      $link = $('div#sudoku_page a.use-ajax');

      // Set resiliant event handlers
      $('#sudoku_page td input').once('rtsudoku-input').each(function() {
        console.log('input');
        $(this).keyup(function(e) {
          $(this).test($(this), e);
        });

        $(this).dblclick(function(e){
          var id = $(this).attr('id');
          var $mi = $('#input-modal');
          var $table = $(this).parent().parent().parent();
          var margin = $(this).parent().height();
          var $offset = $(this).parent().offset();
          var position = $offset.left - $table.offset().left;
          $mi.show();
          $mi.attr('data-attr', id);
          $mi.css('top', $offset.top - $table.offset().top + margin);
          if ($offset.left > $table.width()) {
            position = $offset.left - $table.offset().left - $mi.outerWidth() + $(this).parent().outerWidth();
          }
          $mi.css('left', position);
        });
      });

      // Set modal close event
      $('#input-modal span.close-modal').click(function(e){
        $(this).parent().hide();
      });
      // Set modal number events
      $('#input-modal ul li').once('rtsudoku-li').each(function(){
          $(this).click(function(e){
          e.preventDefault();
          var $wrapper = $(this).parent().parent();
          var $input = $('#' + $wrapper.attr('data-attr'));
          var $value = e.currentTarget.innerHTML;
          e.charCode = 50;
          $('#' + $wrapper.attr('data-attr')).val($value);
          $('td').removeClass('selected');
          $(this).test($input, e);
          $wrapper.hide();
        });
      });

      // Set click event handlers
      $('#toggle_manual').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        $manual = $('div.initial_hidden');
        if ($manual.css('display') == 'none') {
          $manual.show();
          $(this).text(Drupal.t('hide manual'));
        } else {
          $manual.hide();
          $(this).text(Drupal.t('show manual'));
        }
      });

      // Set initial hidden state
      $link.each(function(i) {
        $(this).hide();
        if (i == 1 || i == 4) {
          $(this).show();
        }
      });

      // Set change handler
      $('#edit-level').change(function() {
        level = $(this).val();
        $link.hide();
        $link.each(function(i) {
          if (i == level || i == (parseInt(level) + 3)) {
            $(this).show();
          }
        });
      });


    }
  };

  // Sets initial state for links
  $.fn.initLinks = function() {
    level = $('#edit-level').val();
    $link.hide();
    $link.each(function(i) {
      if (i == level || i == (parseInt(level) + 3)) {
        $(this).show();
      }
    });
  };

  // Inits the game
  $.fn.initSudoku = function(count) {
    countdown = 0;
    contin = true;
    $counter = 0;
    $errors = 0;
    $count = count;
    $('#gf').html($counter);
    $('#error').html($errors);
    contin = false;
    min = 0;
    sec = 0;
    duration = 0;
    if (typeof upd != 'undefined') {
      clearTimeout(upd);
    }
    $(this).setTime();
    $(this).setFocus();
  };

  // Sets the time
  $.fn.setTime = function() {
    min = Math.floor(duration / 60);
    if (min < 10)
      min = '0' + min;
    sec = duration - (min * 60);
    if (sec < 10)
      sec = '0' + sec;
    $("#time").html(min + ':' + sec);
    $(this).getTime();
  };

  // Gets the time
  $.fn.getTime = function() {
    if (duration) {
      duration += 1;
    } else
      duration = 1;
    if ($keepgo) {
      upd = window.setTimeout(function() {
        $(this).setTime();
      }, 1000);
    }
  };

  // Sets the focus to next hidden field
  $.fn.setFocus = function() {

    Out: for (x = 0; x < 9; x++) {

      for (y = 0; y < 9; y++) {
        if ($('#s' + x + y)[0].style.display == 'none') {
          $('#i' + x + y)[0].focus();
          break Out;
        }
      }
    }
  };

  // Checks given field value
  $.fn.test = function(linkobj, evt) {
    var charCode = (evt.charCode) ? evt.charCode : evt.keyCode;

    if (charCode > 36 && charCode < 41) {
      var gk = linkobj.attr('id');
      $(this).moveCursor(gk, charCode);

    } else {
      if ((charCode > 48 && charCode < 58) || (charCode > 96 && charCode < 106)) {
        var input = linkobj.val();
        var kog = linkobj.attr('id');
        var ko = kog.substr(1, 2);
        var field = document.getElementById('t' + ko);
        var number = field.childNodes[0].childNodes[0].nodeValue;
        // since we changed the hidden value in the template to be printed as MD5 hash we have to check with a hashed input
        if (!$(field).hasClass('selected')) {
          $.ajax({
            url : 'sudoku_get_hash/' + input,
            success : function(res) {
              hash = res;

              if (hash == number) {
                var x = ko.substr(0, 1);
                var y = ko.substr(1, 1);
                $counter++;

                var linkl = field.childNodes[1];
                $(linkl).addClass('vanished');
                var field1 = document.getElementById('s' + ko);
                $(field1).text(input);
                field1.style.display = 'inline-block';
                $(field1).addClass('field');
                $('#of').html($count - $counter);
                $('#gf').html($counter);
                $(this).setField(x, y);

                if ($counter == $count) {
                  $keepgo = false;
                  if ($errors == 0) {
                    alert(Drupal
                        .t('Congratulations. You have won without error!'));
                  } else {
                    alert(Drupal.t('You have won!'));
                  }
                }
              } else {
                $errors++;
                er = geffe = $('#error')[0];
                er.innerHTML = $errors;
                if ($errors > 4) {
                  alert(Drupal
                      .t('Sorry you lose. You made more than 4 errors.'));
                  $('#sudoku').remove();
                  $keepgo = false;
                } else {
                  alert(Drupal.t('Error!'));
                  document.getElementById('i' + ko).value = '';
                  document.getElementById('i' + ko).focus();
                }

              }
            }
          });
        }
      } else {
        linkobj.value = '';
      }
    }
  };

  // Moves the cursor according to the key input
  $.fn.moveCursor = function(gk, charC) {

    var x = gk.substr(1, 1);
    var y = gk.substr(2, 1);
    if (x == 'a')
      x = -1;
    if (y == 'a')
      y = -1;

    $('td').removeClass('selected');

    switch (charC) {
    case 37:
      x--;
      if (x < 0) {
        $(this).moveCursor('i9' + y, 37);
      }
      break;

    case 38:
      y--;
      if (y < 0) {
        $(this).moveCursor('i' + x + '9', 38);
      }
      break;

    case 39:
      x++;
      if (x > 8) {
        $(this).moveCursor('ia' + y, 39);
      }
      break;

    case 40:
      y++;
      if (y > 8) {
        $(this).moveCursor('i' + x + 'a', 40);
      }
      break;

    default:
      ;
    }
    $(this).setField(x, y);

  };

  // Sets a solved field
  $.fn.setField = function(x, y) {
    $('#i' + x + y).focus();
    if ($('#s' + x + y).css('display') != 'none') {
      $('#t' + x + y).addClass('selected');
    }
  };

})(jQuery);
