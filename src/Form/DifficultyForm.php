<?php
namespace Drupal\rtsudoku\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class DifficultyForm extends FormBase {
  public function getFormId()
  {
    return 'DifficultyForm';
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $options = array(
        '#type' => 'value',
        '#value' => [
        0 => $this->t('easy'),
        1 => $this->t('normal'),
        2 => $this->t('hard')
      ]
    );

    $form = array();
    $form['level'] = array(
        '#type' => 'select',
        '#title' => $this->t('choos level'),
        '#options' => [$this->t('easy'), $this->t('normal'), $this->t('hard')],
        '#default_value' => '1',
    );

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
  }
}