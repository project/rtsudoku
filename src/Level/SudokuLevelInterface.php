<?php
namespace Drupal\rtsudoku\Level;

/**
 * @file
 *
 * Sudoku level interface.
 */
interface SudokuLevelInterface {

  /**
   * Getter function.
   */
  public function getArguments();
}
