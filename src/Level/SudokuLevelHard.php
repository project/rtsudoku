<?php
namespace Drupal\rtsudoku\Level;
/**
 * @file
 */

use Drupal\rtsudoku\Level\SudokuLevelInterface;
/**
 * Sudoku level hard
 */
class SudokuLevelHard implements SudokuLevelInterface {

  protected  $arguments = array(
    'kofield1' => array(array(0, 0), array(1, 0), array(2, 0), array(0, 1), array(1, 1), array(2, 1), array(0, 2), array(1, 2), array(2, 2)),
    'kofield2' => array(array(3, 0), array(4, 0), array(3, 1), array(4, 1), array(3, 2), array(4, 2)),
    'kofield3' => array(array(0, 3), array(1, 3), array(2, 3), array(0, 4), array(1, 4), array(2, 4)),
    'kofield4' => array(
      /* vertical cross array */
      array(array(3,3), array(3,5), array(5,3), array(5,5)),
      /* diagonal cross array */
      array(array(3,4), array(4,3), array(5,4), array(4,5)),
      /* top left array */
      array(array(3,3), array(3,4), array(5,4), array(5,5)),
      /* top rigth array */
      array(array(5,3), array(5,4), array(3,4), array(3,5)),
      /* left top array */
      array(array(3,3), array(4,3), array(4,5), array(5,5)), 
      /* right top */ 
      array(array(3,5), array(4,5), array(4,3), array(5,3)),
      /* vertical */
      array(array(3, 4), array(4,4), array(5, 4)),
      /* horizontal */
      array(array(4, 3), array(4,4), array(4, 5)),
      /* center */
      array(array(4, 4)),
    ),
    // Start amount: 2 * 4 + 2 * 2 + 2 * 2 + 1 * 4 = 20
    'amounts' => array(4,2,1,3)
  );

  /**
   * Getter function
   */
  public function getArguments() {
    return $this->arguments;
  }

}
