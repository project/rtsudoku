<?php
namespace Drupal\rtsudoku\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\rtsudoku\Sudoku\SudokuInit;

class PageController extends ControllerBase {
  protected $renderer;

  public static function create(ContainerInterface $container) 
  {
    return new static($container->get('renderer'));
  }

  public function __construct(RendererInterface $renderer)
  {
    $this->renderer = $renderer;
  }

  public function getBoard() 
  {
    $new_link = rtsudoku_render_ajax_link();
    $form = \Drupal::formBuilder()->getForm('Drupal\rtsudoku\Form\DifficultyForm');//drupal_get_form('rtsudoku_difficulty_form');
    $select = $this->renderer->render($form);

    $page = [
        '#theme' => 'rtsudoku_page', 
        '#new_link' => $new_link, 
        '#select' => $select,
        '#waiting_text' => RTSUDOKU_WAITING_TEXT,
        '#error_text' => RTSUDOKU_ERROR_INFO,
        '#manual_text' => RTSUDOKU_MANUAL_TEXT,
    ];
    $page['#attached']['library'][] = 'rtsudoku/rtsudoku';
    return $page;

  }

  public function initGeneric($level)
  {
    include_once drupal_get_path('module', 'rtsudoku') . '/includes/rtsudoku_init.inc';
    $response = $this->getBufferedField(Xss::filter($level));
    $ajax_response = new AjaxResponse();
    $ajax_response->addCommand(new ReplaceCommand('#game', '<div id="game">' . $this->renderer->render($response[0]) . '</div>'));
    $ajax_response->addCommand(new ReplaceCommand('#of', '<span id="of">' . $response[1] . '</span>'));
    $ajax_response->addCommand(
      new InvokeCommand(
        NULL, 
        'initSudoku', 
        [$response[1]]
      )
    );
    $ajax_response->addCommand(new InvokeCommand(NULL, 'initLinks'));
    $page = array(
        '#type' => 'ajax',
        '#commands' => $commands,
    );
    return $ajax_response;
  }

  protected function getBufferedField($level)
  {
    /*
     * Timer
     * @todo find a better way to handle timeouts.
     * Forking could be a way, but pcntl is possibly not available.
     */
    set_time_limit(0);
    return $this->getField($level);
  }

  /**
   * Checks the entered value with the hash value.
   *
   * @param int $int
   *   The entered value.
   */
  function getHash($number) {
    print $this->hashField(Xss::filter($number));exit;
  }

  protected function getField($level)
  {
    return SudokuInit::rtsudokuGetField($level);
  }

  protected function hashField($int) {
    return Crypt::hashBase64($int);
  }
  
}