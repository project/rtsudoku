<?php
namespace Drupal\rtsudoku\Sudoku;
/**
 * @file
 */
use Drupal\rtsudoku\Sudoku\SudokuSanitizerInterface;

/**
 * Sudoku board sanitizer
 */
class SudokuBoardSanitizer implements SudokuSanitizerInterface {

  protected $solveBoard = array();
  protected $game;

  /**
   * @see sudokuSanitizerInterface.inc
   */
  public function setConfig(SudokuGameInterface $game, array $solve_board) {
    $this->game = $game;
    $this->solveBoard = $solve_board;
  }

  /**
   * @see sudokuSanitizerInterface.inc
   */
  public function sanitizeBoard() {
    for ($nl = 0; $nl < 9; $nl++) {
      for ($ml = 0; $ml < 9; $ml++) {
        $merk = 0;
        $merker = array();
        for ($o = 0; $o < 9; $o++) {
          if ($this->solveBoard[$nl][$ml][$o] != 0) {
            $merker[$merk] = $o;
            $merk++;
          }
        }
        if ($merk == 1) {
          $this->game->setl($merker[0], $nl, $ml);
        }
      }
    }
    $ntnfield = array();
    for ($m = 0; $m < 9; $m++) {
      for ($n = 0; $n < 9; $n++) {
        for ($p = 1; $p < 10; $p++) {
          if ($this->solveBoard[$m][$n][$p - 1] != 0) {
            // Identify 3x3 field:
            $qm = ceil(($m + 1) / 3);
            $qn = ceil(($n + 1) / 3);
            if (isset($ntnfield[$qm][$qn][$p][0]) && $ntnfield[$qm][$qn][$p][1]['second'][0] == NULL) {
              $ntnfield[$qm][$qn][$p] = array(
                $ntnfield[$qm][$qn][$p][0] + 1,
                array(
                  'first' => array($ntnfield[$qm][$qn][$p][1]['first'][0], $ntnfield[$qm][$qn][$p][1]['first'][1]),
                  'second' => array($m, $n),
                ),
              );
            }
            else {
              $ntnfield[$qm][$qn][$p] = array(
                1,
                array(
                  'first' => array($m, $n),
                  'second' => array(NULL, NULL),
                ),
              );
            }
          }
        }
      }
    }
    // Drop numbers if they ocures more than 2 times-
    $ttcheck = array();
    $tt = array();
    $ntnclean = array();
    reset($ntnfield);
    foreach ($ntnfield as $qm => $array_m) {
      foreach ($array_m as $qn => $array_n) {
        foreach ($array_n as $pl => $array_p) {
          if (isset($ntnfield[$qm][$qn][$pl][0]) && $ntnfield[$qm][$qn][$pl][0] == 2) {
            $ntnclean[$qm][$qn][$pl] = $ntnfield[$qm][$qn][$pl];
          }
          if (isset($ntnfield[$qm][$qn][$pl][0]) && $ntnfield[$qm][$qn][$pl][0] == 1) {
            $deb_in = $pl - 1;
            $this->game->setl($deb_in, $ntnfield[$qm][$qn][$pl][1]['first'][0], $ntnfield[$qm][$qn][$pl][1]['first'][1]);
          }
        }
      }
    }
    reset($ntnclean);
    foreach ($ntnclean as $qmi => $array_mi) {
      foreach ($array_mi as $qni => $array_ni) {
        foreach ($array_ni as $pi => $array_pi) {
          $index = $qm . $qn;
          $value = $pi;
          if (isset($tt[$index]) && $tt[$index]['values'][1] == NULL && $ntnclean[$qmi][$qni][$pi][1]['first'][0] . $ntnclean[$qmi][$qni][$pi][1]['first'][1] . $ntnclean[$qmi][$qni][$pi][1]['second'][0] . $ntnclean[$qmi][$qni][$pi][1]['second'][1] == $ttcheck[$index]) {
            $value1 = $tt[$index]['values'][0];
            $value2 = $value;
          }
          else {
            $value1 = $value;
            $value2 = NULL;
          }
          if ($index) {
            if (isset($ttcheck[$index]) && $ntnclean[$qmi][$qni][$pi][1]['first'][0] . $ntnclean[$qmi][$qni][$pi][1]['first'][1] . $ntnclean[$qmi][$qni][$pi][1]['second'][0] . $ntnclean[$qmi][$qni][$pi][1]['second'][1] == $ttcheck[$index] && $value2 != NULL) {
              $tt[$index] = array(
                'index' => $index,
                'first' => array($ntnclean[$qmi][$qni][$pi][1]['first'][0], $ntnclean[$qmi][$qni][$pi][1]['first'][1]),
                'second' => array($ntnclean[$qmi][$qni][$pi][1]['second'][0], $ntnclean[$qmi][$qni][$pi][1]['second'][1]),
                'values' => array($value1, $value2),
              );
            }
            else {
              $tt[$index] = array(
                'index' => $index,
                'first' => array($ntnclean[$qmi][$qni][$pi][1]['first'][0], $ntnclean[$qmi][$qni][$pi][1]['first'][1]),
                'second' => array($ntnclean[$qmi][$qni][$pi][1]['second'][0], $ntnclean[$qmi][$qni][$pi][1]['second'][1]),
                'values' => array($value1, NULL),
              );
              $ttcheck[$index] = $ntnclean[$qmi][$qni][$pi][1]['first'][0]
              . $ntnclean[$qmi][$qni][$pi][1]['first'][1]
              . $ntnclean[$qmi][$qni][$pi][1]['second'][0]
              . $ntnclean[$qmi][$qni][$pi][1]['second'][1];
            }
          }
        }
      }
    }
    /*
     * Check if numbers they are occuring exact two times
     * per 3x3 field existing in exact two fields.
     */
    $ttclean = array();
    reset($tt);
    foreach ($tt as $tt_key => $tt_a) {
      $ttclean[] = $tt_a;
    }
    for ($i = 0; $i < count($ttclean); $i++) {
      /*
       * If two numbers exists in exact two fields
       * in one 3x3 field they are a "twisted twin".
       * Eleminate all other numbers in the twisted twin fields.
       */
      for ($ix = 1; $ix < 10; $ix++) {
        if ($ttclean[$i]['values'][0] != $ix && $ttclean[$i]['values'][1] != $ix) {
          $this->solveBoard[$ttclean[$i]['first'][0]][$ttclean[$i]['first'][1]][$ix - 1] = 0;
        }
        if ($ttclean[$i]['values'][0] != $ix && $ttclean[$i]['values'][1] != $ix) {
          $this->solveBoard[$ttclean[$i]['second'][0]][$ttclean[$i]['second'][1]][$ix - 1] = 0;
        }
      }
      if ($ttclean[$i]['first'][0] == $ttclean[$i]['second'][0]) {
        for ($iy = 0; $iy < 9; $iy++) {
          if ($iy != $ttclean[$i]['first'][1] && $iy != $ttclean[$i]['second'][1]) {
            for ($p = 1; $p < 10; $p++) {
              if ($p == $ttclean[$i]['values'][0] || $p == $ttclean[$i]['values'][1]) {
                $this->solveBoard[$ttclean[$i]['first'][0]][$iy][$p - 1] = 0;
              }
            }
          }
        }
      }
      if ($ttclean[$i]['first'][1] == $ttclean[$i]['second'][1]) {
        for ($iz = 0; $iz < 9; $iz++) {
          if ($iz != $ttclean[$i]['first'][0] && $iz != $ttclean[$i]['second'][0]) {
            for ($p = 1; $p < 10; $p++) {
              if ($p == $ttclean[$i]['values'][0] || $p == $ttclean[$i]['values'][1]) {
                $this->solveBoard[$iz][$ttclean[$i]['first'][1]][$p - 1] = 0;
              }
            }
          }
        }
      }
    }
    for ($nl = 0; $nl < 9; $nl++) {
      for ($ml = 0; $ml < 9; $ml++) {
        $merk = 0;
        $merker = array();

        for ($o = 0; $o < 9; $o++) {
          if ($this->solveBoard[$nl][$ml][$o] != 0) {
            $merker[$merk] = $o;
            $merk++;
          }
        }
        if ($merk == 1) {
          $this->game->setl($merker[0], $nl, $ml);
        }
      }
    }
    return $this->solveBoard;
  }
}
