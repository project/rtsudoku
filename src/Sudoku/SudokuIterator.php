<?php
namespace Drupal\rtsudoku\Sudoku;

/**
 * @file
 * Sudoku iterator.
 * Enhanced iterator class to provide and handle multi dimensional arrays,
 * as used by the sudoku logic.
 */
class SudokuIterator implements \Iterator {

  /**
   * Class properties
   */
  protected $sudokuBoard = array();
  protected $field = array();

  protected $pos = 0;
  protected $x = 0;
  protected $y = 0;
  protected $lengthX = 9;
  protected $lengthY = 9;
  protected $length = FALSE;
  protected $startIndex = 0;
  // We can assume that a sudoku game allways works
  // with the numbers from 1 to 9.
  protected $numberMin = 1;
  protected $numberMax = 9;

  /**
   * Constructor initially builds a array according to the given parameters
   *
   * @param int $a
   *   A.
   * @param int $b
   *   B.
   * @param int $start_index
   *   Start.
   */
  public function __construct(int $a, int $b, $start_index = FALSE) {

    if (is_int($a) && is_int($b)) {
      $this->lengthX = $a;
      $this->lengthY = $b;
    }

    $this->length = ($this->lengthX * $this->lengthY);

    if ($start_index) {
      $this->startIndex = $start_index;
      $this->x = $start_index;
      $this->y = $start_index;
    }
    else {
      $this->startIndex = 0;
    }

    for ($x = $this->startIndex; $x < $this->lengthX + $this->startIndex; $x++) {
      for ($y = $this->startIndex; $y < $this->lengthY + $this->startIndex; $y++) {
        $this->sudokuBoard[$x][$y] = range($this->numberMin, $this->numberMax);
      }
    }
  }

  /**
   * Implementing Iterator::current()
   */
  public function current() {
    return $this->sudokuBoard[$this->x][$this->y];
  }

  /**
   * Implementing Iterator::next()
   */
  public function next() {
    $this->pos++;
    $this->y = floor($this->pos / $this->length_y) + $this->startIndex;
    $this->x = round($this->pos % $this->length_y) + $this->startIndex;
  }

  /**
   * Implementing Iterator::rewind()
   */
  public function rewind() {
    $this->pos = 0;
  }

  /**
   * Implementing Iterator::key()
   */
  public function key() {
    return $this->pos;
  }

  /**
   * Public function to get X coordinate
   */
  public function keyX() {
    return $this->x;
  }

  /**
   * Public function to get Y coordinate
   */
  public function keyY() {
    return $this->y;
  }

  /**
   * Implementing Iterator::valid()
   */
  public function valid() {
    if ($this->pos < $this->length) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Public function to get the complete board
   */
  public function getBoard() {
    return $this->sudokuBoard;
  }
}
