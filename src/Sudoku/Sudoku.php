<?php
namespace Drupal\rtsudoku\Sudoku;
/**
 * @file
 * Sudoku main class.
 *
 * Generates a valid Sudoku field.
 * A injected tester that extendes the sudokuBase class.
 * Implements the sudokuInterface can build a solvable game.
 * @author Leon Evers (leonevers)
 */
use Drupal\rtsudoku\Sudoku\SudokuRegistry;
use Drupal\rtsudoku\Level\SudokuLevelInterface;

/**
 * Sudoku main class definition.
 */
class Sudoku {
  /**
   * Class properties
   */
  protected $board = array();
  protected $kofield1 = array();
  protected $kofield2 = array();
  protected $kofield3 = array();
  protected $kofield4 = array();
  protected $amounts = array();
  protected $debug = FALSE;

  protected $solvable = FALSE;
  /**
   * limt for inner loop
   * @var int
   */
  protected $limit = 600;
  /**
   * limit for repeated solutions
   * @var int
   */
  protected $solutionLimit = 10;

  protected $iniBoard = array();
  protected $game = NULL;
  protected $level = NULL;

  /**
   * Constructor
   *
   * @param sudokuGameInterface $game
   *   The game object.
   *
   * @param sudokuLevelInterface $level
   *   The level object.
   */
  public function __construct(SudokuGameInterface $game, SudokuLevelInterface $level) {
    $this->game  = $game;
    $this->board = $this->game->makeArray();
    $this->level = $level;

    $arguments = $this->level->getArguments();
    $this->kofield1 = $arguments['kofield1'];
    $this->kofield2 = $arguments['kofield2'];
    $this->kofield3 = $arguments['kofield3'];
    $this->kofield4 = $arguments['kofield4'];
    $this->amounts  = $arguments['amounts'];

    srand((double) microtime(TRUE) * 20000);
  }

  /**
   * Public method to get a complete sudoku game.
   *
   * @return array
   *   [0] => the complete sudoku matrix as array filled with integers
   *   [1] => the matrix with the fields to be exposed
   */
  public function getSudoku() {

    $start = time();
    $this->game->debugger("-- START --", $this->debug);
    $registy = sudokuRegistry::getInstance();

    while (!$this->solvable) {
      $this->game->debugger("OUTER while START", $this->debug);
      $this->buildBoard();
      $this->simplifyField();
      if (!$registy->isNewBoard($this->board)) {
        $this->game->debugger('board is old, continue', $this->debug);
        continue;
      }
      $this->getSymmetricSolution();
    }
    $end = time();
    $duration = $end - $start;

    $this->game->debugger("!! OUTPUT Successful - takes " . $duration . " seconds", $this->debug);
    $this->game->debugger("-- END --", $this->debug);

    return array($this->board, $this->iniBoard);
  }

  /**
   * Initial building of a valid sudoku board
   */
  protected function buildBoard() {
    for ($n = 0; $n < 9; $n++) {
      for ($m = 0; $m < 9; $m++) {
        $mark = 0;
        $temp = array('');
        for ($o = 0; $o < 9; $o++) {
          if ($this->board[$n][$m][$o] != 0) {
            $temp[$mark] = $o;
            $mark++;
          }
        }
        if ($mark === 0) {
          // restart;
          $this->board = $this->game->makeArray();
          $n = 0;
          $m = 0;
          break;

        }
        elseif ($mark === 1) {
          $this->set($temp[0], $n, $m);
        }
        else {
          $s = $this->number($temp);
          $this->set($s, $n, $m);
        }
      }
    }
  }

  /**
   * Gets a arbitrary number from a given array of numbers.
   *
   * @param array $array
   *   Array of numbers.
   *
   * @return int
   *   A random value from the given array.
   */
  protected function number(array $array) {
    $i = rand(0, count($array) - 1);
    $z = $array[$i];
    return $z;
  }

  /**
   * Returns random parts from a given array up to the given limit.
   *
   * @param array $array
   *   Array of number.
   * @param int $count
   *   Amount of numbers to be returned.
   */
  protected function randArray(array $array, int $count) {
    for ($x = 0; $x < $count; $x++) {
      $i = rand(1, count($array)) - 1;
      $erg[] = $array[$i];
      array_splice($array, $i, 1);
    }
    return $erg;
  }

  /**
   * Wrapper function for generic_set.
   *
   * @param int $number
   *   The number to be set.
   * @param int $x
   *   The X-coordinate.
   * @param int $y
   *   The Y-coordinate.
   */
  protected function set(int $number, int $x, int $y) {
    $this->board = $this->game->genericSet($this->board, $number, $x, $y);
  }

  /**
   * Function to simplify field array
   */
  protected function simplifyField() {
    for ($a = 0; $a < 9; $a++) {
      for ($b = 0; $b < 9; $b++) {
        for ($i = 0; $i < 9; $i++) {
          if ($this->board[$a][$b][$i] != 0) {
            $this->board[$a][$b] = $this->board[$a][$b][$i];
          }
        }
      }
    }
  }

  /**
   * Takes the field, tries to find a solvable set of exposed fields.
   * Checks game instance whether the set of fields are valid.
   */
  protected function getSymmetricSolution() {
    $count = 1;
    $solutions = 0;
    // Runs as long as the game is not solvable.
    while (!$this->solvable) {
      if ($count > $this->limit) {
        $this->solvable = FALSE;
        $this->game->debugger("INNER ABORT after " . $count . " loops . ", $this->debug);
        break;
      }
      if ($solutions >= $this->solutionLimit) {
        $this->solvable = FALSE;
        $this->game->debugger("SOLUTION ABORT after " . $solutions . " solution repeatitions . ", $this->debug);
        break;
      }
      $this->iniBoard = array();
      $ifield = array();
      /*
       * Config definitions moved to constructor
       * and can be changed by the client code.
       */
      $ifield1 = $this->randArray($this->kofield1, $this->amounts[0]);
      $ifield2 = $this->randArray($this->kofield2, $this->amounts[1]);
      $ifield3 = $this->randArray($this->kofield3, $this->amounts[2]);
      $ifield4 = $this->randArray($this->kofield4, $this->amounts[3]);

      for ($i = 0; $i < count($ifield1); $i++) {
        $ifield[] = $ifield1[$i];
        $ifield[] = array($ifield1[$i][0], (8 - $ifield1[$i][1]));
        $ifield[] = array((8 - $ifield1[$i][0]), $ifield1[$i][1]);
        $ifield[] = array((8 - $ifield1[$i][0]), (8 - $ifield1[$i][1]));
      }
      for ($i = 0; $i < count($ifield2); $i++) {
        $ifield[] = $ifield2[$i];
        $ifield[] = array($ifield2[$i][0], (8 - $ifield2[$i][1]));
        $ifield[] = array((8 - $ifield2[$i][0]), $ifield2[$i][1]);
        $ifield[] = array((8 - $ifield2[$i][0]), (8 - $ifield2[$i][1]));
      }
      for ($i = 0; $i < count($ifield3); $i++) {
        $ifield[] = $ifield3[$i];
        $ifield[] = array($ifield3[$i][0], (8 - $ifield3[$i][1]));
        $ifield[] = array((8 - $ifield3[$i][0]), $ifield3[$i][1]);
        $ifield[] = array((8 - $ifield3[$i][0]), (8 - $ifield3[$i][1]));
      }
      for ($i = 0; $i < count($ifield4[0]); $i++) {
        $ifield[] = $ifield4[0][$i];
      }

      for ($a = 0; $a < 9; $a++) {
        for ($b = 0; $b < 9; $b++) {
          $check = TRUE;
          for ($i = 0; $i < count($ifield); $i++) {
            if ($ifield[$i][0] == $a && $ifield[$i][1] == $b) {
              $check = FALSE;
              break;
            }
          }
          if ($check) {
            $this->iniBoard[$a][$b] = TRUE;
          }
        }
      }

      $registry = sudokuRegistry::getInstance();
      if (!$registry->isNewGame($this->board, $ifield)) {
        $this->game->debugger("solution is old, continue", $this->debug);
        $solutions++;
        continue;
      }

      // Terminates the loop if true.
      $this->solvable = $this->game->isSolvable($this->board, $ifield);
      $count++;
    }
  }
}
