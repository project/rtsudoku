<?php
namespace Drupal\rtsudoku\Sudoku;
/**
 * @file
 * 
 * Interface definition.
 */
interface SudokuSanitizerInterface {

  /**
   * Set parameters.
   *
   * @param Object $game
   *   Type sudokuGame.
   * @param array $solve_board
   *   Solve borad.
   */
  public function setConfig(SudokuGameInterface $game, array $solve_board);

  /**
   * Solves all fields due to twisted pairs, twisted triples and streets
   */
  public function sanitizeBoard();
}
