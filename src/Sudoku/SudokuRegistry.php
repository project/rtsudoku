<?php
namespace Drupal\rtsudoku\Sudoku;

/**
 * @file
 *
 * Enhanced Registry class
 * To store and provide games and solutions.
 * Public wrappers can check if a given value is allready processed.
 */
class SudokuRegistry {

  /**
   * Class constants.
   */
  const SUDOKU_GAME = 'game';
  const SUDOKU_BOARD = 'board';

  /**
   * Class properties.
   */
  protected static $instance = NULL;
  protected $values = array();

  /**
   * Static method to get the singelton instance.
   *
   * @return object
   *   Registry instance.
   */
  public static function getInstance() {

    if (self::$instance === NULL) {
      self::$instance = new self();
    }

    return self::$instance;
  }

  /**
   * Constructor.
   */
  protected function __construct() {
  }

  /**
   * Private clonig.
   */
  private function __clone() {
  }

  /**
   * Generic setter.
   *
   * @param string $key
   *   Key.
   * @param mixed $value
   *   Value.
   */
  protected function set($key, $value) {
    $this->values[$key][$value] = TRUE;
  }

  /**
   * Generic getter.
   *
   * @param string $key
   *   Key.
   * @param mixed $value
   *   Value.
   *
   * @return mixed
   *   Returns false or the value.
   */
  protected  function get($key, $value) {
    if (isset($this->values[$key][$value])) {
      return $this->values[$key][$value];
    }
    return FALSE;
  }

  /**
   * Concrete getter for a complete game.
   *
   * @param array $board
   *   The board array.
   * @param array $inifield
   *   The array of initially open fields.
   */
  protected function getGame(array $board, array $inifield) {
    return $this->get(self::SUDOKU_GAME, self::makeSignature($board, $inifield));
  }

  /**
   * Concrete setter for a complete game.
   *
   * @param array $board
   *   The board array.
   * @param array $inifield
   *   The array of initially open fields.
   */
  protected function setGame(array $board, array $inifield) {
    $this->set(self::SUDOKU_GAME, self::makeSignature($board, $inifield));
  }

  /**
   * Public wrapper function to check if a given game is not yet rejected.
   *
   * @param array $board
   *   The board array.
   * @param array $inifield
   *   The array of initially open fields.
   *
   * @return mixed
   *   Returns false or the value.
   */
  public function isNewGame(array $board, array $inifield) {
    if (!$this->getGame($board, $inifield)) {
      $this->setGame($board, $inifield);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Concrete getter for a board.
   *
   * @param array $board
   *   The board.
   */
  protected function getBoard(array $board) {
    return $this->get(self::SUDOKU_BOARD, self::makeSignature($board));
  }

  /**
   * Concrete setter for a board.
   *
   * @param unknown_type $board
   *   The board.
   */
  protected function setBoard(array $board) {
    $this->set(self::SUDOKU_BOARD, self::makeSignature($board));
  }

  /**
   * Public wrapper function to check wether a given board is not yet rejected.
   *
   * @param array $board
   *   The board.
   */
  public function isNewBoard(array $board) {
    if (!$this->getBoard($board)) {
      $this->setBoard($board);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Helper function to build the signature .
   *
   * @param array $board
   *   The board.
   * @param array $inifield
   *   The initally open fields.
   *
   * @return string
   *   The flatened array.
   */
  protected function makeSignature(array $board, $inifield = FALSE) {
    $signature = '';
    foreach ($board as $row) {
      foreach ($row as $field) {
        $signature .= (string) $field;
      }
    }
    if ($inifield) {
      foreach ($inifield as $fields) {
        foreach ($fields as $field) {
          $signature .= (string) $field;
        }
      }
    }
    return $signature;
  }

}
