<?php
namespace Drupal\rtsudoku\Sudoku;

/**
 * @file
 * Interface implemented by classes which should be injected
 * into a sudoku instance.
 */
interface SudokuGameInterface {

  /**
   * Checks solvability of a complete game.
   *
   * @param array $board
   *   Board.
   * @param array $solution
   *   Solution.
   *
   * @return boolean
   *   Whether board is solvable.
   */
  public function isSolvable(array $board, array $solution);

  /**
   * Helper function for building a 2 dimensional array
   * with a range from 1 - 9 for all 9 times 9 fields.
   *
   * @return array
   *   The field base array.
   */
  public function makeArray();

  /**
   * Sets a given number at the given coordinates and
   * eliminates all conflicting possibilities.
   *
   * @param array $board
   *   The array that is manipulated.
   * @param int $number
   *   The number to be set.
   * @param int $x
   *   X-coordinate.
   * @param int $y
   *   Y-coordinate.
   */
  public function genericSet(array $board, int $number, int $x, int $y);

}
