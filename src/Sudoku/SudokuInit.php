<?php
namespace Drupal\rtsudoku\Sudoku;

use Drupal\rtsudoku\Sudoku\Sudoku;
use Drupal\rtsudoku\Sudoku\SudokuGame;
use Drupal\rtsudoku\Level\SudokuLevelEasy;
use Drupal\rtsudoku\Level\SudokuLevelNormal;
use Drupal\rtsudoku\Level\SudokuLevelHard;

/**
 * @file
 * Rtsudoku init file.
 */
class SudokuInit {
  /**
   * Get field function, returns the level object.
   *
   * @param int $level
   *   The level: 0 - 2 allowed.
   *
   * @return object
   *   The level object.
   */
  public static function rtsudokuGetField($level)
  {
    switch ($level) {
      case '0':
        $level_class = new SudokuLevelEasy();
        break;

      case '1':
        $level_class = new SudokuLevelNormal();
        break;

      case '2':
        $level_class = new SudokuLevelHard();
        break;
    }

    $sudoku = new Sudoku(new SudokuGame(new SudokuBoardSanitizer()), $level_class);
    $game = $sudoku->getSudoku();
    $count = 0;
    foreach($game[1] as $row){
      $count += count($row);
    }
    $markup = self::rtsudokuBuildTable($game);
    $theme = [
        '#theme' => 'rtsudoku_game',
        '#board' => ['#children' => $markup]
    ];

    return array($theme, $count);
  }

  /**
   * Builds the table markup
   * @param array $game
   * @return string
   */
  protected static function rtsudokuBuildTable($game)
  {
    $board = $game[0];
    $start_template = $game[1];
    $response = '';

    for ($y = 0; $y < 9; $y++) {
      $response .= "<tr class=\"row_" . $y . "\">\n";
      for ($x = 0; $x < 9; $x++) {
        if ($x == 2 || $x == 5) {
          if ($y == 2 || $y == 5) {
            $response .= '<td id="t' . $x . $y . '" class="td1 field_' . $x . '_' . $y . '">';
          }
          else {
            $response .= '<td id="t' . $x . $y . '" class="td2 field_' . $x . '_' . $y;
            if ($y == 8) {
              $response .= ' border-bottom-solid';
            }
            $response .= '">';
          }
        }
        else {
          if ($y == 2 || $y == 5) {
            $response .= '<td id="t' . $x . $y . '" class="td3 field_' . $x . '_' . $y;
            if ($x == 8) {
              $response .= ' border-right-solid';
            }
            $response .= '">';
          }
          else {
            $response .= '<td id="t' . $x . $y . '" class="td4 field_' . $x . '_' . $y;
            if ($x == 8) {
              $response .= ' border-right-solid';
            }
            if ($y == 8) {
              $response .= ' border-bottom-solid';
            }
            $response .= '">';
          }
        }
        if (isset($start_template[$x][$y])) {
          $response .= "<span id=\"s" . $x . $y . "\" class=\"field\" style=\"display: none;\">";
          $response .= rtsudoku_hash_field($board[$x][$y]);
          $response .= "</span><input id=\"i" . $x . $y . "\" size=\"1\" class=\"input-field\"/></td>\n";
        }
        else {
          $response .= "<span id=\"s" . $x . $y . "\" class=\"field\">";
          $response .= $board[$x][$y];
          $response .= "</span><input id=\"i" . $x . $y . "\" size=\"1\" class=\"input-field vanished\" /></td>\n";
        }
      }
      $response .= "</tr>\n";
    }
    return $response;
  }
}
