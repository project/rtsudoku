<?php
namespace Drupal\rtsudoku\Sudoku;
/**
 * @file
 * Sudoku game class.
 */
use Drupal\rtsudoku\Sudoku\SudokuBase;
use Drupal\rtsudoku\Sudoku\SudokuBoardSanitizer;

/**
 * This class is used with a sudoku object
 * to find a solvable configuration of exposed fields.
 */
class SudokuGame extends SudokuBase {
  /**
   * Class properties
   */
  protected $solveBoard = array();
  protected $solution;
  protected $board;
  protected $sanitizer;

  /**
   * Constructor.
   *
   * @param SudokuBoardSanitizer $sanitizer
   *   Sanitizer object.
   */
  public function __construct(SudokuBoardSanitizer $sanitizer) {
    $this->sanitizer = $sanitizer;
  }

  /**
   * Wrapper function for sudokuBase::genericSet
   *
   * @param int $number
   *   Number tobe set.
   * @param int $x
   *   The x coordinate.
   * @param int $y
   *   The y coordinate.
   */
  public function setl(int $number, int $x, int $y) {
    $this->solveBoard = $this->genericSet($this->solveBoard, $number, $x, $y);
  }

  /**
   * @see sudokuBase.inc
   */
  public function isSolvable(array $board, array $solution) {
    $this->solveBoard = $this->makeArray();
    $this->board = $board;
    $this->solution = $solution;

    // Set initial fields.
    for ($ix = 0; $ix < count($this->solution); $ix++) {
      $x = $this->solution[$ix][0];
      $y = $this->solution[$ix][1];
      $this->setl($this->board[$x][$y] - 1, $x, $y);
    }

    // Start depending solvabilities.
    for ($nl = 0; $nl < 9; $nl++) {
      for ($ml = 0; $ml < 9; $ml++) {
        $mark = 0;
        $temp = array();

        for ($o = 0; $o < 9; $o++) {
          if ($this->solveBoard[$nl][$ml][$o] != 0) {
            $temp[$mark] = $o;
            $mark++;
          }
        }
        if ($mark == 1) {
          $this->setl($temp[0], $nl, $ml);
        }
      }
    }
    // Rows and columns.
    for ($nl = 0; $nl < 9; $nl++) {
      // Initialize temporary markers.
      for ($i = 1; $i < 10; $i++) {
        ${'r_' . $i} = 0;
        ${'s_' . $i} = 0;
      }

      for ($ol = 0; $ol < 9; $ol++) {
        // Solve rows.
        for ($ii = 1; $ii < 10; $ii++) {
          if ($this->solveBoard[$nl][$ol][$ii - 1] != 0) {
            ${'r_' . $ii}++;
            if (${'r_' . $ii} == 1) {
              ${'ko' . $ii . '_rx'} = $nl;
              ${'ko' . $ii . '_ry'} = $ol;
            }
          }
        }

        // Solve columns.
        for ($ii = 1; $ii < 10; $ii++) {
          if ($this->solveBoard[$ol][$nl][$ii - 1] != 0) {
            ${'s_' . $ii}++;
            if (${'s_' . $ii} == 1) {
              ${'ko' . $ii . '_sx'} = $ol;
              ${'ko' . $ii . '_sy'} = $nl;
            }
          }
        }
      }

      // Clean up all dependencies due to found row or column solutions.
      for ($i = 1; $i < 10; $i++) {
        if (${'r_' . $i} == 1) {
          $this->setl($i - 1, ${'ko' . $i . '_rx'}, ${'ko' . $i . '_ry'});
        }
        if (${'s_' . $i} == 1) {
          $this->setl($i - 1, ${'ko' . $i . '_sx'}, ${'ko' . $i . '_sy'});
        }
      }
    }

    // Clean up fields due to their area that has a solution.
    for ($a = 0; $a < 9; $a++) {
      for ($i = 1; $i < 10; $i++) {
        ${'r_' . $i} = 0;
      }

      for ($b = 0; $b < 9; $b++) {
        for ($i = 1; $i < 10; $i++) {
          if ($this->solveBoard[$this->quadMatrix[$a][$b][0]][$this->quadMatrix[$a][$b][1]][$i - 1] != 0) {
            ${'r_' . $i}++;
            if (${'r_' . $i} == 1) {
              ${'ko' . $i . '_rx'} = $this->quadMatrix[$a][$b][0];
              ${'ko' . $i . '_ry'} = $this->quadMatrix[$a][$b][1];
            }
          }
        }
      }

      // Clean up.
      for ($i = 1; $i < 10; $i++) {
        if (${'r_' . $i} == 1) {
          $this->setl($i - 1, ${'ko' . $i . '_rx'}, ${'ko' . $i . '_ry'});
        }
      }
    }

    $this->sanitizer->setConfig($this, $this->solveBoard);
    $this->solveBoard = $this->sanitizer->sanitizeBoard();

    // Check for possible solvability of complete board.
    for ($n = 0; $n < 9; $n++) {
      for ($m = 0; $m < 9; $m++) {
        $mark = 0;
        $temp = array();

        for ($o = 0; $o < 9; $o++) {
          if ($this->solveBoard[$n][$m][$o] != 0) {
            $temp[$mark] = $o;
            $mark++;
          }
        }

        if ($mark != 1) {
          return FALSE;
        }
      }

      if ($n == 8) {
        return TRUE;
      }
    }
  }
}
